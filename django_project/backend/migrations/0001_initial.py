# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Categories'
        db.create_table(u'backend_categories', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'backend', ['Categories'])

        # Adding model 'Keywords'
        db.create_table(u'backend_keywords', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'backend', ['Keywords'])

        # Adding model 'POI'
        db.create_table(u'backend_poi', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('location', self.gf('django.contrib.gis.db.models.fields.PointField')()),
            ('areaOfInterest', self.gf('django.contrib.gis.db.models.fields.PolygonField')()),
        ))
        db.send_create_signal(u'backend', ['POI'])

        # Adding M2M table for field categories on 'POI'
        m2m_table_name = db.shorten_name(u'backend_poi_categories')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('poi', models.ForeignKey(orm[u'backend.poi'], null=False)),
            ('categories', models.ForeignKey(orm[u'backend.categories'], null=False))
        ))
        db.create_unique(m2m_table_name, ['poi_id', 'categories_id'])

        # Adding M2M table for field keywords on 'POI'
        m2m_table_name = db.shorten_name(u'backend_poi_keywords')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('poi', models.ForeignKey(orm[u'backend.poi'], null=False)),
            ('keywords', models.ForeignKey(orm[u'backend.keywords'], null=False))
        ))
        db.create_unique(m2m_table_name, ['poi_id', 'keywords_id'])

        # Adding model 'Ad'
        db.create_table(u'backend_ad', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('location', self.gf('django.contrib.gis.db.models.fields.PointField')()),
            ('areaOfInterest', self.gf('django.contrib.gis.db.models.fields.PolygonField')()),
        ))
        db.send_create_signal(u'backend', ['Ad'])

        # Adding M2M table for field categories on 'Ad'
        m2m_table_name = db.shorten_name(u'backend_ad_categories')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('ad', models.ForeignKey(orm[u'backend.ad'], null=False)),
            ('categories', models.ForeignKey(orm[u'backend.categories'], null=False))
        ))
        db.create_unique(m2m_table_name, ['ad_id', 'categories_id'])

        # Adding M2M table for field keywords on 'Ad'
        m2m_table_name = db.shorten_name(u'backend_ad_keywords')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('ad', models.ForeignKey(orm[u'backend.ad'], null=False)),
            ('keywords', models.ForeignKey(orm[u'backend.keywords'], null=False))
        ))
        db.create_unique(m2m_table_name, ['ad_id', 'keywords_id'])


    def backwards(self, orm):
        # Deleting model 'Categories'
        db.delete_table(u'backend_categories')

        # Deleting model 'Keywords'
        db.delete_table(u'backend_keywords')

        # Deleting model 'POI'
        db.delete_table(u'backend_poi')

        # Removing M2M table for field categories on 'POI'
        db.delete_table(db.shorten_name(u'backend_poi_categories'))

        # Removing M2M table for field keywords on 'POI'
        db.delete_table(db.shorten_name(u'backend_poi_keywords'))

        # Deleting model 'Ad'
        db.delete_table(u'backend_ad')

        # Removing M2M table for field categories on 'Ad'
        db.delete_table(db.shorten_name(u'backend_ad_categories'))

        # Removing M2M table for field keywords on 'Ad'
        db.delete_table(db.shorten_name(u'backend_ad_keywords'))


    models = {
        u'backend.ad': {
            'Meta': {'object_name': 'Ad'},
            'areaOfInterest': ('django.contrib.gis.db.models.fields.PolygonField', [], {}),
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['backend.Categories']", 'symmetrical': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['backend.Keywords']", 'symmetrical': 'False'}),
            'location': ('django.contrib.gis.db.models.fields.PointField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'backend.categories': {
            'Meta': {'object_name': 'Categories'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'backend.keywords': {
            'Meta': {'object_name': 'Keywords'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'backend.poi': {
            'Meta': {'object_name': 'POI'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'areaOfInterest': ('django.contrib.gis.db.models.fields.PolygonField', [], {}),
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['backend.Categories']", 'symmetrical': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['backend.Keywords']", 'symmetrical': 'False'}),
            'location': ('django.contrib.gis.db.models.fields.PointField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['backend']