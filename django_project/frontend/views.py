from django.views.generic import TemplateView, DetailView

from backend.models import POI


class Homepage(TemplateView):
    template_name = 'homepage.html'


class ListPage(TemplateView):
    template_name = 'listpage.html'


class PoiDetailView(DetailView):
    model = POI
    template_name = 'poi.html'
