from django.contrib import admin
from backend.models import POI, Categories, Keywords
# class aModelAdmin(admin.ModelAdmin):
#    pass

# admin.site.register(aModel, aModelAdmin):
#    pass


class POIAdmin(admin.ModelAdmin):
    pass


class CategoriesAdmin(admin.ModelAdmin):
    pass


class KeywordsAdmin(admin.ModelAdmin):
    pass

admin.site.register(POI, POIAdmin)
admin.site.register(Categories, CategoriesAdmin)
admin.site.register(Keywords, KeywordsAdmin)
