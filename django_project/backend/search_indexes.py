from haystack import indexes
from .models import POI, Ad


class POIIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True, model_attr='name')
    keywords = indexes.MultiValueField()

    def get_model(self):
        return POI

    def prepare_keywords(self, obj):
        return [keyword.name for keyword in obj.keywords.all()]


class AdIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True, model_attr='name')
    keywords = indexes.MultiValueField()

    def get_model(self):
        return Ad

    def prepare_keywords(self, obj):
        return [keyword.name for keyword in obj.keywords.all()]
