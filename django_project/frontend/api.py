from django.conf.urls import url
from django.contrib.gis.geos import Polygon, Point
from haystack.query import SearchQuerySet
from tastypie.resources import ModelResource
from tastypie import fields
from tastypie.utils import trailing_slash
from tastypie.paginator import Paginator
from backend.models import (
    POI,
    Categories,
    Keywords,
    Worktime,
    Ad,
    Price,
    FeaturePanel,
    Place
)


class CategoriesResource(ModelResource):

    class Meta:
        queryset = Categories.objects.all()
        resource_name = 'categories'


class KeywordsResource(ModelResource):

    class Meta:
        queryset = Keywords.objects.all()
        resource_name = 'keywords'


class POIResource(ModelResource):
    open_until = fields.CharField(attribute='openUntil', readonly=True)
    formated_hours = fields.ListField(attribute='formatedHours', readonly=True)
    categories = fields.ManyToManyField(CategoriesResource, 'categories')
    keywords = fields.ManyToManyField(KeywordsResource, 'keywords')

    #Second parameter comes from poiInstance.worktime_set!
    worktime = fields.ToManyField(
        'frontend.api.WorktimeResource', 'worktime_set', null=True, full=True
    )
    prices = fields.ToManyField(
        'frontend.api.PriceResource', 'price_set', null=True, full=True
    )

    class Meta:
        queryset = POI.objects.all()
        resource_name = 'poi'
        limit = 0

    def prepend_urls(self):
        return [
            url(
                r"^(?P<resource_name>%s)/search%s$" % (
                    self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_search'),
                name="api_get_search"),
        ]

    def get_search(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.throttle_check(request)

        # Do the query.
        search = request.GET.get('search')
        sqs = SearchQuerySet().models(POI).filter(
            keywords=search).filter_or(content=search)

        objects = []

        for result in sqs:
            bundle = self.build_bundle(obj=result.object, request=request)
            bundle = self.full_dehydrate(bundle)
            objects.append(bundle)

        object_list = {
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list)

    def build_filters(self, filters=None):
        if filters is None:
            filters = {}
        orm_filters = super(POIResource, self).build_filters(filters)

        if('bbox' in filters):
            v_tmp = filters.get('bbox').split(',')
            orm_filters.update({
                'location__within': Polygon.from_bbox(v_tmp)
            })

        if('category' in filters):
            orm_filters.update({
                'categories__id__in': (
                    [int(x) for x in filters.get('category').split(',')])
            })
        return orm_filters

    def dehydrate_images(self, bundle):
        """
        Image URLs in 'files_widget.ImagesField' are stored in single string
        delimited by \n, split string on \n. Prepend /media/ to all image URLs.
        Return empty string if no images exist.
        """
        imageUrls = bundle.data['images'].split('\n')
        if bundle.data['images'] != '':
            return map(lambda img: '/media/' + img, imageUrls)
        else:
            return ''

    def get_object_list(self, request):
        """
        Use bbox centroid to sort points by their distance from centroid.
        """
        strBbox = request.GET.get('bbox')
        if strBbox:
            tupleBbox = strBbox.split(',')
            bbox = Polygon.from_bbox(tupleBbox)
            query = super(POIResource, self).get_object_list(request)
            return (
                query.distance(bbox.centroid).order_by('distance')
            )
        else:
            return super(POIResource, self).get_object_list(request)


class WorktimeResource(ModelResource):

    class Meta:
        queryset = Worktime.objects.all()
        resource_name = 'worktime'


class AdResource(ModelResource):
    categories = fields.ManyToManyField(CategoriesResource, 'categories')
    keywords = fields.ManyToManyField(KeywordsResource, 'keywords')

    class Meta:
        queryset = Ad.objects.all()
        resource_name = 'ad'
        limit = 0

    def prepend_urls(self):
        return [
            url(
                r"^(?P<resource_name>%s)/search%s$" % (
                    self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_search'),
                name="api_get_search_ad"),
        ]

    def get_search(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.throttle_check(request)

        # Do the query.
        search = request.GET.get('search')
        sqs = SearchQuerySet().models(Ad).filter(
            keywords=search).filter_or(content=search)

        objects = []

        for result in sqs:
            bundle = self.build_bundle(obj=result.object, request=request)
            bundle = self.full_dehydrate(bundle)
            objects.append(bundle)

        object_list = {
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list)

    def build_filters(self, filters=None):
        if filters is None:
            filters = {}
        orm_filters = super(AdResource, self).build_filters(filters)

        if('category' in filters):
            orm_filters.update({
                'categories__id__in': (
                    [int(x) for x in filters.get('category').split(',')])
            })

        if('bbox' in filters):
            v_tmp = filters.get('bbox').split(',')
            orm_filters.update({
                'location__within': Polygon.from_bbox(v_tmp)
            })

        return orm_filters

    def dehydrate_images(self, bundle):
        """
        Image URLs in 'files_widget.ImagesField' are stored in single string
        delimited by \n, split string on \n. Prepend /media/ to all image URLs.
        Return empty string if no images exist.
        """
        imageUrls = bundle.data['images'].split('\n')
        if bundle.data['images'] != '':
            return map(lambda img: '/media/' + img, imageUrls)
        else:
            return ''

    def get_object_list(self, request):
        """
        Use bbox centroid to sort points by their distance from centroidself.
        """
        strBbox = request.GET.get('bbox')
        if strBbox:
            tupleBbox = strBbox.split(',')
            bbox = Polygon.from_bbox(tupleBbox)
            query = super(AdResource, self).get_object_list(request)
            return (
                query.distance(bbox.centroid).order_by('distance')
            )
        else:
            return super(AdResource, self).get_object_list(request)


class PriceResource(ModelResource):
    formatedToddler = fields.CharField(
        attribute='formatedToddler', readonly=True)
    formatedChildren = fields.CharField(
        attribute='formatedChildren', readonly=True)
    formatedAdults = fields.CharField(
        attribute='formatedAdults', readonly=True)
    formatedStudents = fields.CharField(
        attribute='formatedStudents', readonly=True)
    formatedSeniors = fields.CharField(
        attribute='formatedSeniors', readonly=True)

    class Meta:
        queryset = Price.objects.all()
        resource_name = 'prices'


class POIListResource(ModelResource):
    open_until = fields.CharField(attribute='openUntil', readonly=True)
    keywords = fields.ManyToManyField(KeywordsResource, 'keywords')

    class Meta:
        queryset = POI.objects.all()
        resource_name = 'poilist'
        paginator_class = Paginator

    def prepend_urls(self):
        return [
            url(
                r"^(?P<resource_name>%s)/search%s$" % (
                    self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_search'),
                name="api_get_search_list"),
        ]

    def get_search(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.throttle_check(request)

        # Do the query.
        search = request.GET.get('search')
        sqs = SearchQuerySet().models(POI).filter(
            keywords=search).filter_or(content=search)

        objects = []

        for result in sqs:
            bundle = self.build_bundle(obj=result.object, request=request)
            bundle = self.full_dehydrate(bundle)
            objects.append(bundle)

        object_list = {
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list)

    def dehydrate_images(self, bundle):
        return bundle.obj.imagesThumbs()

    def get_object_list(self, request):

        loc = request.GET.get('loc')
        if loc:
            temp = loc.split(',')
            center = Point(float(temp[1]), float(temp[0]))
            query = super(POIListResource, self).get_object_list(request)
            return (
                query.distance(center).order_by('distance')
            )
        else:
            return super(POIListResource, self).get_object_list(request)


class FeatureResource(ModelResource):

    class Meta:
        queryset = FeaturePanel.objects.all().order_by('order')
        resource_name = 'feature'


class PlaceResource(ModelResource):

    class Meta:
        queryset = Place.objects.all()
        resource_name = 'place'
        limit = 0

    def build_filters(self, filters=None):
        if filters is None:
            filters = {}
        orm_filters = super(PlaceResource, self).build_filters(filters)

        if('bbox' in filters):
            v_tmp = filters.get('bbox').split(',')
            orm_filters.update({
                'location__within': Polygon.from_bbox(v_tmp)
            })

        return orm_filters

    def get_object_list(self, request):
        """
        Use bbox centroid to sort points by their distance from centroid.
        """
        query = super(PlaceResource, self).get_object_list(request)
        strPOIId = request.GET.get('poiID')

        # Get places within 1 mile (1609 meters) from referent poi.
        if strPOIId:
            poi = POI.objects.get(pk=int(strPOIId))
            query = query.filter(
                location__distance_lte=(poi.location, 1609)
            )

        return query
