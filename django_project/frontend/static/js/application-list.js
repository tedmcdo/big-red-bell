APP.POI = Backbone.Model.extend({
    url:'/api/v1/poilist/'
});

APP.FeaturePanel = Backbone.Model.extend({
    url:'/api/v1/feature/'
});

APP.POICollection = Backbone.Collection.extend({
    url: '/api/v1/poilist/',
    model: APP.POI,
    meta: {},

    parse: function(response) {
        this.meta = response.meta;
        return response.objects;
    },
});

APP.FeaturePanelCollection = Backbone.Collection.extend({
    url: '/api/v1/feature/',
    model: APP.FeaturePanel,
    meta: {},

    parse: function(response) {
        this.meta = response.meta;
        return response.objects;
    },
});


APP.POIItemView = Backbone.View.extend({
    tagName: 'li',
    className: 'poi',
    initialize: function() {

    },
    render: function() {
        $(this.el).html(JST.list_poi_tmpl({model: this.model}));
        if (this.model.get('largeBox')) {
            $(this.el).addClass('large');
        }

        return this;
    }
});

APP.FeaturePanelItemView = Backbone.View.extend({
    tagName: 'div',
    className: 'poi full',
    initialize: function(options) {
        this.options = options;
    },
    render: function() {
        $(this.el).html(JST.list_feature_tmpl({
            model: this.model,
            class_first: (this.options.options.poi_count === 0) ? 'first' : ''
        }));
        return this;
    }
});

APP.POIView = Backbone.View.extend({
    el: $("#locations"),
    initialize: function(options) {
        var self = this;
        this.$el.packery({
            // options
            itemSelector: '.poi',
            transitionDuration: '0s'
        });


        this.offset = 0;
        this.limit = 8;
        // number of POIs currently displayed
        this.PoiCount = 0;
        // remember which panel was last displayed
        this.FeaturePanelDisplayed = 0;
        this.features = new APP.FeaturePanelCollection();
        this.features.fetch();

        this.collection = new APP.POICollection();
        var data = {
            'offset': self.offset,
            'limit': self.limit
        };
        if (APP.location) {
            data['loc'] = APP.location.coords.latitude + ',' + APP.location.coords.longitude;
        }
        this.collection.fetch({
            data: data,
            success: $.proxy(self.render, self)
        });
        $('#load_more').on('click', $.proxy(this.loadNext, self));
    },
    render: function(elements) {
        var self = this;
        _(elements.models).each(function(item){
            if (this.PoiCount === 0 || this.PoiCount % 8 === 0) {
                self.displayFeaturePanel(this.PoiCount);
            }
            var html = [];
            html.push(self.renderItem(item));
            this.$el.append(html).packery( 'appended', html );
            APP.initLightSlider(item.id);
            this.PoiCount += 1;
        },this);
        this.offset += this.limit;
        if (this.collection.meta.total_count < this.offset) {
            $('#load_more').hide();
        }

    },
    renderItem: function(item) {
        var myItem = new APP.POIItemView({
            model:item,
            collection:this.collection
        });
        return myItem.render().el;
    },
    displayFeaturePanel: function(PoiCount) {
        var item = this.features.at(this.FeaturePanelDisplayed);
        if (typeof item !== 'undefined') {
            var html = [];
            html.push(this.renderFeaturePanel(item, PoiCount));
            this.$el.append(html).packery( 'appended', html );
            this.FeaturePanelDisplayed += 1;
        }
    },
    renderFeaturePanel: function(item, PoiCount) {
        var myItem = new APP.FeaturePanelItemView({
            model:item,
            collection:this.collection,
            options: {'poi_count': PoiCount}
        });
        return myItem.render().el;
    },
    loadNext: function() {
        var self = this;
        var data = {
            'offset': self.offset,
            'limit': self.limit
        };
        if (APP.location) {
            data['loc'] = APP.location.coords.latitude + ',' + APP.location.coords.longitude;
        }
        this.collection.fetch({
            data: data,
            success: $.proxy(self.render, self)
        });
    }
});


APP.initLightSlider = function(poiID) {
    $('#listpage-lightSlider-' + poiID).lightSlider({
      gallery: false,
      item: 1,
      slideMargin: 0,
      galleryMargin: 10,
      thumbMargin: 10,
      thumbItem: 10,
      pager: false,
      responsive : [
        {
          breakpoint:760,
          settings: {
            slideMargin: 0,
            galleryMargin: 10,
            thumbMargin: 10,
            thumbItem: 6
          }
        },
        {
          breakpoint:480,
          settings: {
            slideMargin: 0,
            galleryMargin: 10,
            thumbMargin: 10,
            thumbItem: 4
          }
        }
      ]
    });
}


if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(displayWithLocation, errorLocation);
} else {
    APP.location = null;
    var poiview = new APP.POIView();
}

function displayWithLocation(location) {
    APP.location = location;
    var poiview = new APP.POIView();
}

function errorLocation(error) {
    APP.location = null;
    var poiview = new APP.POIView();
}
