import logging
logger = logging.getLogger(__name__)

import django.forms as forms
from django.contrib.gis import forms as gisForms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from django_select2 import AutoModelSelect2TagField, ModelSelect2MultipleField

from .models import (
    POI,
    Keywords,
    Categories,
    Ad,
    FeaturePanel,
    Amenity,
    Place
)

# from crispy_forms.layout import (
#     Layout,
#     Submit,
#     Div,
#     HTML,
#     Field,
#     Button
# )
# from crispy_forms.bootstrap import (
#     FormActions,
#     Tab,
#     TabHolder
# )


class KeywordChoices(AutoModelSelect2TagField):
    queryset = Keywords.objects
    search_fields = ['name__icontains']

    def get_model_field_values(self, value):
        return {'name': value}


class CustomOLWidget(gisForms.OpenLayersWidget):
    template_name = 'OL-widget.html'


class POIForm(forms.ModelForm):
    #ModelSelect2MultipleField and KeywordChoices do extra processing when you
    #call form.save()! Calling form.save(commit=False) and saving object
    #manually will not save values from ModelSelect2MultipleField and
    #KeywordChoices.
    categories = ModelSelect2MultipleField(queryset=Categories.objects)
    keywords = KeywordChoices()
    amenities = forms.ModelMultipleChoiceField(
        widget=forms.CheckboxSelectMultiple,
        queryset=Amenity.objects.all(), required=False)

    #This field will be filled with JSON object containing worktime of POI.
    #We MUST add "autocomplete=off" to it's widget or else browser will
    #autocomplete values from previous sessions.
    worktime = forms.CharField(
        widget=forms.HiddenInput(attrs={'autocomplete': 'off'})
    )

    price = forms.CharField(
        widget=forms.HiddenInput(attrs={'autocomplete': 'off'})
    )

    class Meta:
        model = POI
        #Widget will always return data in 3857, we need to tell Django to
        #convert it to 4326. Define SRID of fields as 4326.
        #Define widgets to operate on 3857. Other combinations won't work.
        #THIS SETTINGS ONLY WORK WELL WITH CUSTOM OLMapWidget!
        #See - OLMapWidget_custom.js -> MapWidget.prototype.read_wkt
        location = gisForms.PointField(srid=4326)
        areaOfInterest = gisForms.PolygonField(srid=4326)
        widgets = {
            'location': CustomOLWidget(attrs={'map_srid': 3857}),
            'areaOfInterest': CustomOLWidget(attrs={'map_srid': 3857})
        }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.html5_required = False
        self.helper.form_class = 'form-horizontal poiform'
        self.helper.label_class = 'col-md-3'
        self.helper.field_class = 'col-md-4 col-md-offset-1'
        self.helper.add_input(Submit('submit', 'Submit'))

        self.base_fields['categories'].help_text = (
            'Select from existing categories'
        )
        self.base_fields['keywords'].help_text = (
            'Select from existing categories or add new'
        )

        self.base_fields['description'].label = (
            'Description (HTML)'
        )

        super(POIForm, self).__init__(*args, **kwargs)


class AdForm(forms.ModelForm):
    categories = ModelSelect2MultipleField(queryset=Categories.objects)
    keywords = KeywordChoices()

    class Meta:
        model = Ad
        #See comment above.
        location = gisForms.PointField(srid=4326)
        areaOfInterest = gisForms.PolygonField(srid=4326)
        widgets = {
            'location': CustomOLWidget(attrs={'map_srid': 3857}),
            'areaOfInterest': CustomOLWidget(attrs={'map_srid': 3857}),
        }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.html5_required = False
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3'
        self.helper.field_class = 'col-md-4 col-md-offset-1'
        self.helper.add_input(Submit('submit', 'Submit'))

        self.base_fields['categories'].help_text = (
            'Select from existing categories'
        )
        self.base_fields['keywords'].help_text = (
            'Select from existing categories or add new'
        )

        super(AdForm, self).__init__(*args, **kwargs)


class CategoriesForm(forms.ModelForm):
    class Meta:
        model = Categories

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.html5_required = False
        self.helper.form_class = 'form-horizontal'
        self.helper.add_input(Submit('submit', 'Submit'))

        super(CategoriesForm, self).__init__(*args, **kwargs)


class LoginBackend(forms.Form):
    username = forms.CharField()
    password = forms.CharField(
        widget=forms.TextInput(attrs={'type': 'password'})
    )

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.html5_required = False
        self.helper.form_class = 'form-horizontal'
        self.helper.form_method = 'post'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-md-4'
        self.helper.add_input(Submit('submit', 'Submit'))

        super(LoginBackend, self).__init__(*args, **kwargs)

    def clean(self):
        """
        Check if user is active and staff member. If not display
        appropriate message.
        """
        cleaned_data = super(LoginBackend, self).clean()
        username = cleaned_data['username']
        password = cleaned_data['password']

        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise forms.ValidationError("Username does not exist.")
        if not user.is_active:
            raise forms.ValidationError("User is not active.")
        elif not user.is_staff:
            raise forms.ValidationError("User is not a staff member.")
        elif not authenticate(username=username, password=password):
            raise forms.ValidationError("Username/password mismatch.")
        else:
            return cleaned_data


class FeaturePanelForm(forms.ModelForm):
    class Meta:
        model = FeaturePanel

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.html5_required = False
        self.helper.form_class = 'form-horizontal'
        self.helper.add_input(Submit('submit', 'Submit'))

        super(FeaturePanelForm, self).__init__(*args, **kwargs)


class AmenityForm(forms.ModelForm):
    class Meta:
        model = Amenity

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.html5_required = False
        self.helper.form_class = 'form-horizontal'
        self.helper.add_input(Submit('submit', 'Submit'))

        super(AmenityForm, self).__init__(*args, **kwargs)


class PlaceForm(forms.ModelForm):

    class Meta:
        model = Place
        #Widget will always return data in 3857, we need to tell Django to
        #convert it to 4326. Define SRID of fields as 4326.
        #Define widgets to operate on 3857. Other combinations won't work.
        #THIS SETTINGS ONLY WORK WELL WITH CUSTOM OLMapWidget!
        #See - OLMapWidget_custom.js -> MapWidget.prototype.read_wkt
        location = gisForms.PointField(srid=4326)
        widgets = {
            'location': CustomOLWidget(attrs={'map_srid': 3857})
        }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.html5_required = False
        self.helper.form_class = 'form-horizontal poiform'
        self.helper.label_class = 'col-md-3'
        self.helper.field_class = 'col-md-4 col-md-offset-1'
        self.helper.add_input(Submit('submit', 'Submit'))

        super(PlaceForm, self).__init__(*args, **kwargs)
