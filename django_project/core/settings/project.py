from .contrib import *
import os

DATABASES = {
    'default': {
        # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'ENGINE': 'django.db.backends.',
        # Or path to database file if using sqlite3.
        'NAME': 'brb_dev',
        # The following settings are not used with sqlite3:
        'USER': '',
        'PASSWORD': '',
        # Empty for localhost through domain sockets or '127.0.0.1' for
        # localhost through TCP.
        'HOST': '',
        # Set to empty string for default.
        'PORT': '',
    }
}

# Project apps
INSTALLED_APPS += (
    'haystack',
    'backend',
    'django_select2',
    'sorl.thumbnail',
    'topnotchdev.files_widget',
    'frontend',
    'tastypie',
)

# Set debug to false for production
DEBUG = TEMPLATE_DEBUG = THUMBNAIL_DEBUG = False


PIPELINE_JS = {
    'contrib': {
        'source_filenames': (
            'js/jquery-2.1.1.min.js',
            'js/csrf-ajax.js',
            'js/underscore-min.js',
            'js/bootstrap.min.js',
        ),
        'output_filename': 'js/contrib.js',
    },
    'backend': {
        'source_filenames': (
            'js/jquery-2.1.1.min.js',
            'js/csrf-ajax.js',
            'js/underscore-min.js',
            'js/bootstrap.min.js',
        ),
        'output_filename': 'js/backend.js',
    },
    'frontend': {
        'source_filenames': (
            'js/backbone.js',
            'js/jstemplates/*.jst',
            'js/mapbox.js',
            'js/leaflet-omnivore.min.js',
            'js/init_project.js',
            'js/application.js',
            'js/royalslider.min.js',
            'js/leaflet.markercluster.js'
        ),
        'output_filename': 'js/frontend.js',
    },
    'frontend-list': {
        'source_filenames': (
            'js/backbone.js',
            'js/packery.js',
            'js/jstemplates/*.jst',
            'js/jquery.lightSlider.min.js',
            'js/init_project.js',
            'js/application-list.js',
        ),
        'output_filename': 'js/frontend.js',
    },
    'backend_form': {
        'source_filenames': (
            'js/jquery-ui.min_1.10.3.js',
            'js/moment.min_2.6.0.js',
        ),
        'output_filename': 'js/backend_form.js',
    },
    'frontend-poi-detail': {
        'source_filenames': (
            'js/packery.js',
            'js/jquery.lightSlider.min.js',

            'js/backbone.js',
            'js/jstemplates/*.jst',
            'js/mapbox.js',
            'js/leaflet-omnivore.min.js',
            'js/init_project.js',
            'js/leaflet.markercluster.js',

            'js/application-poi-detail.js'
        ),
        'output_filename': 'css/frontend.js',
    },
}

PIPELINE_CSS = {
    'backend': {
        'source_filenames': (
            'css/bootstrap.min.css',
            'css/backend.css',
            'css/backend_poiform.css'
            # 'css/bootstrap-responsive.min.css',
        ),
        'output_filename': 'css/contrib.css',
        'extra_context': {
            'media': 'screen, projection',
        }
    },
    'contrib': {
        'source_filenames': (
            'css/bootstrap.min.css',
        ),
        'output_filename': 'css/contrib.css',
    },
    'frontend-map': {
        'source_filenames': (
            'css/main.css',
            'css/mapbox.css',
            'css/MarkerCluster.css',
            'css/app.css',
        ),
        'output_filename': 'css/frontend.css',
    },
    'frontend-list': {
        'source_filenames': (
            'css/new-main.css',
            'css/app.css',
        ),
        'output_filename': 'css/frontend.css',
    },
    'frontend-poi-detail': {
        'source_filenames': (
            'css/mapbox.css',
            'css/MarkerCluster.css',
            'css/new-main.css',
            'css/app.css',
            'css/poi-detail.css'
        ),
        'output_filename': 'css/frontend.css',
    },
}

CRISPY_TEMPLATE_PACK = 'bootstrap3'

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'xapian_backend.XapianEngine',
        'PATH': os.path.join(os.path.dirname(__file__), 'xapian_index')
    },
}
HAYSTACK_DEFAULT_OPERATOR = 'AND'
HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'
