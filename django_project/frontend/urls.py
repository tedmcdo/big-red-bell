from django.conf.urls import patterns, url, include
from tastypie.api import Api
from .api import (
    POIResource,
    CategoriesResource,
    KeywordsResource,
    WorktimeResource,
    AdResource,
    POIListResource,
    FeatureResource,
    PlaceResource
)
from .views import (
    Homepage,
    ListPage,
    PoiDetailView
)


v1_api = Api(api_name='v1')
v1_api.register(POIResource())
v1_api.register(CategoriesResource())
v1_api.register(KeywordsResource())
v1_api.register(WorktimeResource())
v1_api.register(AdResource())
v1_api.register(POIListResource())
v1_api.register(FeatureResource())
v1_api.register(PlaceResource())


urlpatterns = patterns(
    '',
    url(r'^api/', include(v1_api.urls)),

    url(r'^map/$', Homepage.as_view(), name='homepage'),
    url(r'^$', ListPage.as_view(), name='listpage'),
    url(r'^poi/(?P<pk>\d+)/', PoiDetailView.as_view(), name='poi_detail_page'),


)
