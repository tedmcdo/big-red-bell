import logging
logger = logging.getLogger(__name__)
import datetime
from collections import namedtuple
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.gis.db import models

from topnotchdev import files_widget


class Categories(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name


class Keywords(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name


class POI(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(null=True, blank=True)
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=200, null=True, blank=True)
    largeBox = models.BooleanField(default=False)
    location = models.PointField(srid=4326)
    areaOfInterest = models.PolygonField(srid=4326)
    categories = models.ManyToManyField('Categories')
    keywords = models.ManyToManyField('Keywords')
    images = files_widget.ImagesField(null=True, blank=True)
    amenities = models.ManyToManyField('Amenity')

    objects = models.GeoManager()

    def __unicode__(self):
        return self.name

    def imagesThumbs(self):
        images = [[img.url, img.thumbnail('400x400').url] for img in (
            self.images.all())]
        return images

    def similarPois(self):
        """
        This function returns 4 POIs similar to referent POI.
        1) Find POIs that share at least one category with referent POI.
        2) Count how many keywords each similar POI shares with referent POI.
           extendedSimilarPoi named tuples are created on the way
           (with poi and countSharedKeywords data).
        3) Sort similar POIs by countSharedKeywords, limit to 4 POIs and
           extract poi from extendedSimilarPoi named tuples.
        """
        # Find POIs that share at least one category with referent POI.
        similarByCategory = POI.objects.filter(
            categories__id__in=self.categories.values_list('id')
        ).distinct().exclude(pk=self.pk)

        # For each POI similar by category count how many keywords
        # it shares with referent POI.
        referentPoiKeywords = self.keywords.values_list('id')
        extendedSimilarPoisList = []
        extendedSimilarPoi = namedtuple(
            'extendedSimilarPoi',
            'poi countSharedKeywords'
        )
        for poi in similarByCategory:
            countSharedKeywords = 0
            for keyword in poi.keywords.values_list('id'):
                if keyword in referentPoiKeywords:
                    countSharedKeywords += 1

            # Append similar POI (with matches info) to extendedSimilarPoisList
            extendedSimilarPoisList.append(
                extendedSimilarPoi(poi, countSharedKeywords)
            )

        return [sorted_poi.poi for sorted_poi in sorted(
            extendedSimilarPoisList,
            key=lambda similarPoi: similarPoi.countSharedKeywords,
            reverse=True)[:4]
        ]

    def openUntil(self):
        now = datetime.datetime.now()
        try:
            wt = Worktime.objects.get(
                poiid=self.pk,
                dow=now.weekday(),
                startTime__lte=now.time(),
                endTime__gte=now.time()
            )
            return 'open until {}'.format(wt.endTime.strftime('%I:%M%p'))
        except ObjectDoesNotExist:
            return 'Closed'

    def formatedHours(self):
        DAYS = (
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday'
        )
        hours = Worktime.objects.filter(poiid=self.pk).order_by('dow')
        fhours = []
        start_day = -1
        end_day = -1
        start_hour = -1
        end_hour = -1
        for hour in hours:
            text = {}
            if (hour.startTime != start_hour or hour.endTime != end_hour):
                if (start_day != -1):
                    if (start_day != end_day):
                        text['days'] = '{} - {}'.format(
                            DAYS[start_day][:3],
                            DAYS[end_day][:3]
                        )
                        if (start_hour != end_hour):
                            text['hours'] = '{} - {}'.format(
                                start_hour.strftime('%I:%M%p'),
                                end_hour.strftime('%I:%M%p')
                            )
                        else:
                            text['hours'] = 'Closed'
                    else:
                        text['days'] = '{}'.format(
                            DAYS[start_day],
                        )
                        if (start_hour != end_hour):
                            text['hours'] = '{} - {}'.format(
                                start_hour.strftime('%I:%M%p'),
                                end_hour.strftime('%I:%M%p')
                            )
                        else:
                            text['hours'] = 'Closed'
                    fhours.append(text)
                start_day = hour.dow
                end_day = hour.dow
                start_hour = hour.startTime
                end_hour = hour.endTime
            else:
                end_day = hour.dow
        text = {}
        if (start_day != end_day):
            text['days'] = '{} - {}'.format(
                DAYS[start_day][:3],
                DAYS[end_day][:3]
            )
            if (start_hour != end_hour):
                text['hours'] = '{} - {}'.format(
                    start_hour.strftime('%I:%M%p'),
                    end_hour.strftime('%I:%M%p')
                )
            else:
                text['hours'] = 'Closed'
        else:
            text['days'] = '{}'.format(
                DAYS[start_day],
            )
            if (start_hour != end_hour):
                text['hours'] = '{} - {}'.format(
                    start_hour.strftime('%I:%M%p'),
                    end_hour.strftime('%I:%M%p')
                )
            else:
                text['hours'] = 'Closed'
        fhours.append(text)
        return fhours


class Ad(models.Model):
    name = models.CharField(max_length=200)
    url = models.URLField(
        max_length=500,
        null=True,
        blank=True
    )
    location = models.PointField(srid=4326)
    areaOfInterest = models.PolygonField(srid=4326)
    categories = models.ManyToManyField('Categories')
    keywords = models.ManyToManyField('Keywords')
    images = files_widget.ImageField(null=True, blank=True)

    objects = models.GeoManager()

    def __unicode__(self):
        return self.name


class Worktime(models.Model):
    MONDAY = 0
    TUESDAY = 1
    WEDNESDAY = 2
    THURSDAY = 3
    FRIDAY = 4
    SATURDAY = 5
    SUNDAY = 6

    DAYS = (
        (MONDAY, 'Monday'),
        (TUESDAY, 'Tuesday'),
        (WEDNESDAY, 'Wednesday'),
        (THURSDAY, 'Thursday'),
        (FRIDAY, 'Friday'),
        (SATURDAY, 'Saturday'),
        (SUNDAY, 'Sunday')
    )

    poiid = models.ForeignKey('POI')
    dow = models.SmallIntegerField(choices=DAYS, default=MONDAY)
    startTime = models.TimeField()
    endTime = models.TimeField()


class Price(models.Model):
    toddler = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    children = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    adults = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    students = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    seniors = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    poi = models.ForeignKey('POI')

    def formatedToddler(self):
        if (self.toddler == 0):
            return 'FREE'
        else:
            return '${}'.format(self.toddler)

    def formatedChildren(self):
        if (self.children == 0):
            return 'FREE'
        else:
            return '${}'.format(self.children)

    def formatedAdults(self):
        if (self.adults == 0):
            return 'FREE'
        else:
            return '${}'.format(self.adults)

    def formatedStudents(self):
        if (self.students == 0):
            return 'FREE'
        else:
            return '${}'.format(self.students)

    def formatedSeniors(self):
        if (self.seniors == 0):
            return 'FREE'
        else:
            return '${}'.format(self.seniors)


class FeaturePanel(models.Model):
    name = models.CharField(max_length=200)
    html = models.TextField()
    order = models.SmallIntegerField(default=0)

    def __unicode__(self):
        return self.name


class Amenity(models.Model):
    name = models.CharField(max_length=200)
    icon = files_widget.ImageField()
    order = models.SmallIntegerField(default=0)

    def __unicode__(self):
        return self.name


class Place(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(null=True, blank=True)
    location = models.PointField(srid=4326)

    objects = models.GeoManager()

    def __unicode__(self):
        return self.name
