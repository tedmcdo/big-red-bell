from django.db import models
from haystack import signals
from haystack.exceptions import NotHandled
from .models import POI


class CustomRealtimeSignalProcessor(signals.BaseSignalProcessor):
    def setup(self):
        # Naive (listen to all model saves).
        models.signals.post_save.connect(self.handle_save)
        models.signals.post_delete.connect(self.handle_delete)
        models.signals.m2m_changed.connect(
            self.handle_m2msave, sender=POI.keywords.through)
        # Efficient would be going through all backends & collecting all models
        # being used, then hooking up signals only for those.

    def teardown(self):
        # Naive (listen to all model saves).
        models.signals.post_save.disconnect(self.handle_save)
        models.signals.post_delete.disconnect(self.handle_delete)
        models.signals.m2m_changed.disconnect(
            self.handle_m2mdelete, sender=POI.keywords.through)
        # Efficient would be going through all backends & collecting all models
        # being used, then disconnecting signals only for those.

    def handle_m2msave(self, sender, instance, **kwargs):
        """
        Given an individual model instance, determine which backends the
        update should be sent to & update the object on those backends.
        """
        using_backends = self.connection_router.for_write(instance=instance)

        for using in using_backends:
            try:
                index = (
                    self.connections[using].get_unified_index()
                    .get_index(instance.__class__))
                index.update_object(instance, using=using)
            except NotHandled:
                # TODO: Maybe log it or let the exception bubble?
                pass
