APP.category = [];
APP.dontReloadNext = false;
APP.searchActive = false;
APP.selectedPOI = false;

APP.POI = Backbone.Model.extend({
    url:'/api/v1/poi/'
});

APP.POICollection = Backbone.Collection.extend({
    url: '/api/v1/poi/',
    model: APP.POI,
    meta: {},

    parse: function(response) {
        this.meta = response.meta;
        return response.objects;
    },
});

APP.AD = Backbone.Model.extend({
    url:'/api/v1/ad/'
});

APP.ADCollection = Backbone.Collection.extend({
    url: '/api/v1/ad/',
    model: APP.AD,
    meta: {},

    parse: function(response) {
        this.meta = response.meta;
        return response.objects;
    },
});

APP.POIItemView = Backbone.View.extend({
    tagName: 'li',
    className: 'list-bg cf',
    events: {
        'click': "showDetailed",
        'mouseover': "highlightPOI",
        'mouseout': "unhighlightPOI",
    },
    initialize: function() {
        this.popup=document.createElement('div');
        this.popup.className='popup';
    },
    render: function() {
        $(this.el).html(JST.poi_tmpl({model: this.model}));
        return this;
    },
    renderDetail: function() {
        var html = JST.poi_detail_tmpl({model: this.model});
        $APP.trigger('addPOIDetail', {'html': html});
    },
    showDetailed: function() {
        var selected = null;
        if (APP.selectedPOI == null) {
            APP.returnCenter = APP.map.getCenter();
            APP.returnZoom = APP.map.getZoom();
        } else {
            // fetch selected model from collection
            selected = poiview.collection.get(APP.selectedPOI);
        }
        APP.selectedPOI = this.model.get('id');
        if (selected) selected.marker.myItem.unhighlightPOI();
        this.highlightPOI();
        APP.dontReloadNext = true;
        this.renderDetail();
        this.zoomTo();
    },
    highlightPOI: function() {
        this.$el.children('.list').addClass('list-hover');
        this.model.marker.setIcon(L.divIcon({
            className: 'count-icon-highlight-new',
            html: this.model.attributes.number,
            iconAnchor: [21,43],
            iconSize: [43, 47]
        }));
        var parent = poiview.markerLayer.getVisibleParent(this.model.marker);
        if (_.isUndefined(parent._group)) {
            this.showPopup();
        } else {
            $(parent._icon).addClass('active');
        }
    },
    unhighlightPOI: function() {
        if (APP.selectedPOI != this.model.get('id')) {
            this.$el.children('.list').removeClass('list-hover');
            this.model.marker.setIcon(L.divIcon({
                className: 'count-icon-new',
                html: this.model.attributes.number,
                iconAnchor: [21,43],
                iconSize: [43, 47]
            }));
            var parent = poiview.markerLayer.getVisibleParent(this.model.marker);
            if (_.isUndefined(parent._group)) {
                $(this.popup).remove();
            } else {
                $(parent._icon).removeClass('active');
            }
        }
    },
    showPopup: function() {
        $('.home').append(this.popup);
        var ll = this.model.marker.getLatLng();
        var point = APP.map.latLngToContainerPoint(ll);
        var html = '<p class="popup_elem">'+this.model.attributes.name+'</p>';
        $(this.popup).css(
            {
                'top':(point.y- 90) +'px',
                'left': point.x-100 + 'px'
            }
        );
        $(this.popup).html(html);
    },
    zoomTo: function() {
        var ll = this.model.marker.getLatLng();
        if (APP.mobile) {
            var box = 0.0002;
        } else {
            var box = 0.002;
        }
        var southWest = L.latLng(ll.lat + box, ll.lng + box);
        var northEast = L.latLng(ll.lat - box, ll.lng - box);
        var bounds = L.latLngBounds(southWest, northEast);
        var options = { paddingTopLeft: APP.sidebarWidthPoint };
        if (APP.mobile)
            options.paddingBottomRight = APP.sidebarHeightPoint
        APP.map.fitBounds(bounds, options);
    }
});

APP.ADItemView = Backbone.View.extend({
    tagName: 'div',
    className: 'ad',
    render: function() {
        $(this.el).html(JST.ad_tmpl({model: this.model}));
        return this;
    }
});

APP.POIView = Backbone.View.extend({
    el: $(".main-panel-map"),

    initialize: function(options) {
        this.collection = new APP.POICollection();
        this.collection.bind('reset', this.render, this);
        this.collection.bind('remove', this.render, this);
        this.ads = new APP.ADCollection();
        $APP.on('submitSearch', $.proxy(this.loadPOIs, this));
        $APP.on('addPOIDetail', $.proxy(this.addPOIDetail, this));
        if ($( window ).width() < 860) {
            APP.mobile = true;
            APP.sidebarWidthPoint = [0, $('.header').height()];
            APP.sidebarHeightPoint = [0, ($(window).height() / 2)];
        } else {
            APP.mobile = false;
            APP.sidebarWidthPoint = L.point($('.main-panel-map').outerWidth() + 20, $('.header').height());
        }
        this.initMap();
        APP.selectedPOI = null;
        APP.returnCenter = null;
        APP.returnZoom = null;
    },

    render: function() {
        if (_.isUndefined(this.markerLayer)) {
            this.markerLayer = new L.MarkerClusterGroup({
                showCoverageOnHover: false,
                iconCreateFunction: function(cluster) {
                    return new L.divIcon({
                        className: 'count-icon-cluster',
                        html: cluster.getAllChildMarkers()[0].number+'<em class="clusterChildCount">'+cluster.getChildCount()+'</em>',
                        iconAnchor: [25,49],
                        iconSize: [47, 51]
                     });
                }
            });
        } else {
            this.markerLayer.clearLayers();
        }
        $('.ad').remove();
        $('.locations').remove();
        $('.popup').remove();

        this.number = 0;
        var self=this;
        _(this.ads.models).each(function(item) {
            var adhtml = self.renderAd(item);
            this.$el.append(adhtml);
            var poihtml  = self.renderPOI();
            this.$el.append(poihtml);
        }, this);

        if (this.collection.models.length > this.number ) {
            var start = this.number;
            var ul=document.createElement('ul');
            ul.className='locations cf';
            for (var i = start; i < this.collection.models.length; i++) {
                var item = this.collection.models[i];
                item.attributes.number = this.number + 1;
                myItem = this.renderItem(item, ul);
                item.marker = this.renderMarker(item);
                item.marker.myItem = myItem;
                this.number += 1;
                if (APP.selectedPOI == myItem.model.get('id')) {
                    myItem.highlightPOI();
                    //myItem.renderDetail();
                }
            }
            this.$el.append(ul);
        }
        this.markerLayer.addTo(this.map);
        //Scroll item list to top on each update.
        //window.scrollTo(0, 0);
        $('.la-anim').removeClass('la-animate');
        return this;
    },

    renderPOI: function() {
        if (this.collection.models.length > this.number ) {
            var ul=document.createElement('ul');
            ul.className='locations cf';
            var start = this.number;
            for (var i = start; i < start + 4; i++) {
                if (!_.isUndefined(this.collection.models[i])) {
                    var item = this.collection.at(i);
                    item.attributes.number = this.number + 1;
                    myItem = this.renderItem(item, ul);
                    item.marker = this.renderMarker(item);
                    item.marker.myItem = myItem;
                    this.number += 1;
                    if (APP.selectedPOI == myItem.model.get('id')) {
                        myItem.highlightPOI();
                        //myItem.renderDetail();
                    }
                }
            };
            return ul;
        } else {
            return '';
        }
    },

    renderItem: function(item, ul) {
        var myItem = new APP.POIItemView({
            model:item,
            collection:this.collection
        });
        ul.appendChild(myItem.render().el);
        return myItem;
    },

    renderAd: function(item) {
        var myItem = new APP.ADItemView({
            model:item,
            collection:this.ads
        });
        return myItem.render().el;
    },

    renderMarker: function(item) {
        var data = item.pick('id', 'name', 'location', 'number', 'address');
        //Omnivore returns layer which contains marker we need to set custom marker...
        var wkt = omnivore.wkt.parse(data.location);
        //Get marker.
        var layer = wkt.getLayers()[0];
        //As we can't use layer.setIcon without breaking something, construct a new marker.
        var marker = new L.Marker(layer.getLatLng(), {
             icon: L.divIcon({
                className: 'count-icon-new',
                html: data.number,
                iconAnchor: [21,43],
                iconSize: [43, 47]

             })
        });
        marker.number = data.number;
        marker.addTo(this.markerLayer);
        $(marker).hover(
            function() {
                this.myItem.highlightPOI();
            },
            function() {
                this.myItem.unhighlightPOI();
            }
        );
        $(marker).click(function() {
            this.myItem.showDetailed();
        });
        return marker;
    },

    addPOIDetail: function(event, data) {
        var self = this;
        $('.poi-bg').remove();
        this.$el.prepend(data.html);
        if (APP.mobile)
            this.$el.addClass('top50');
        $('#gallery-poi').royalSlider(sliderOptions);
        window.scrollTo(0, 0);
        $('#close').click(function() {
            self.closeDetailPOI();
        })
    },

    closeDetailPOI: function() {
        var selected = this.collection.get(APP.selectedPOI);
        APP.selectedPOI = null;
        if (!_.isUndefined(selected)) selected.marker.myItem.unhighlightPOI();
        APP.dontReloadNext = false;
        $('.poi-bg').remove();
        APP.map.setZoom(APP.returnZoom);
        if (APP.mobile)
            this.$el.removeClass('top50');
    },

    initMap: function() {
        var self = this;
        this.moved = 0;
        //Init map.
        this.map = L.mapbox.map('map', 'tedmcdo.ippbgho2', {'minZoom': 12, 'touchZoom': true, 'tapTolerance': 25});

        //On any movement, get data from server and show it as markers on map.
        //Bound box is limited to visible map.
        this.map.on('moveend', function() {
            // if true, moveend is cause by show detailed and we don't want to reload
            if (!APP.dontReloadNext && !APP.searchActive) {
                self.loadPOIs()
            } else {
                // set to false as next action will be pan or zoom and we want reload to fire
                APP.dontReloadNext = false;
            }
        }, this);
        //this.map.on('zoomend', this.loadPOIs, this);
        this.map.on('movestart', function() {
            $('.popup').remove();
        }, this);
        this.map.on('dragend', function(e) {
            this.moved =  this.moved + e.distance
            var limit = $( window ).width() * 0.25;
            if (this.moved < limit) {
                APP.dontReloadNext = true;
            } else {
                this.moved = 0;
            }
        }, this);

        //Set view AFTER 'load' listner is defined.
        this.map.fitBounds([
            [47.4819684, -122.459696],
            [47.7341388, -122.224433]
            ], { 'paddingTopLeft': APP.sidebarWidthPoint });
        APP.map = this.map;
    },

    loadPOIs: function() {
        $('.la-anim').addClass('la-animate');
        var self = this;
        var poiurl = this.collection.url;
        var adurl = this.ads.url;
        var searchString = $('#searchInput').val();
        if (searchString != '') {
            if (APP.selectedPOI != null) {
                this.closeDetailPOI();
                APP.dontReloadNext = true;
            }
            APP.searchActive = true;
            filters = { 'search': searchString };
            this.collection.url = poiurl + 'search/';
            this.ads.url = adurl + 'search/';
        } else if (APP.category.length != 0) {
            APP.searchActive = true;
            if (APP.selectedPOI != null) {
                this.closeDetailPOI();
                APP.dontReloadNext = true;
            }
            filters = { 'category': APP.category.join() };
        } else {
            APP.searchActive = false;
            bbox = this.getBBOX();
            filters = { 'bbox': bbox };
        }
        this.ads.fetch({
            'reset':true,
            data: filters,
            success: function() {
                self.collection.fetch({
                    'reset':true,
                    data: filters
                });
                self.collection.url = poiurl;
                self.ads.url = adurl;
            }
        });
    },

    getBBOX: function() {
        // TODO: rewrite
        if (APP.mobile) {
            var mapBounds = this.map.getBounds();
        } else {
            //Get current bound box for sidebar.
            var mapBounds = this.map.getBounds();
            //Extract south west lat lng from bound box and convert it to point.
            var pointSW = this.map.latLngToLayerPoint(mapBounds.getSouthWest());
            //Get width of sidebar and its left offset, then combine them to create a point object.
            var offset = this.$el.offset();
            var width = this.$el.outerWidth();
            var leftOffset = parseFloat(offset['left']);
            var elemWidth = L.point(leftOffset + width + 20, 0);
            //Add constructed point object to point object extracted from map bounds.
            //Then convert resulting point object to latlng object which becomes new bound box.
            var offsetPointSW = pointSW.add(elemWidth);
            var latLngSW = this.map.layerPointToLatLng(offsetPointSW);
            mapBounds._southWest = latLngSW;

            //Get current bound box for header.
            var pointNE = this.map.latLngToLayerPoint(mapBounds.getNorthEast());
            var headerHeight = $('.header').height();
            var elemHeight = L.point(0, headerHeight);
            var heightOffset = pointNE.add(elemHeight);
            var latLngNE = this.map.layerPointToLatLng(heightOffset);
            mapBounds._northEast = latLngNE;
        }
        return mapBounds.toBBoxString();
    }
});

var poiview = new APP.POIView();
$('form').on('submit', function(event) {
    event.preventDefault();
    resetActive();
    $APP.trigger('submitSearch');
});
var sliderOptions = {
    fullscreen: {
      enabled: false,
      nativeFS: true
    },
    controlNavigation: 'thumbnails',
    autoScaleSlider: true,
    autoScaleSliderWidth: 600,
    autoScaleSliderHeight: 450,
    imageScalePadding: 0,
    loop: false,
    imageScaleMode: 'fit-if-smaller',
    navigateByClick: true,
    numImagesToPreload:2,
    arrowsNav:true,
    arrowsNavAutoHide: true,
    arrowsNavHideOnTouch: true,
    keyboardNavEnabled: true,
    fadeinLoadedSlide: true,
    globalCaption: false,
    globalCaptionInside: false,
    thumbs: {
      appendSpan: true,
      firstMargin: true,
      paddingBottom: 4
    }
  };

function resetActive() {
        APP.category = [];
        $('#events-filter').children('img').attr('src', '/static/img/events.svg');
        $('#sports-filter').children('img').attr('src', '/static/img/sports.svg');
        $('#parks-filter').children('img').attr('src', '/static/img/parks.svg');
        $('#animals-filter').children('img').attr('src', '/static/img/animals.svg');
        $('#attractions-filter').children('img').attr('src', '/static/img/attractions.svg');
        $('#museum-filter').children('img').attr('src', '/static/img/museums.svg');
    }

jQuery(document).ready(function($) {
    $("a.toggle-filter").click(function () {
        $(".toggle-filter").toggleClass("toggle-filter-active");
        $(".filters").toggleClass("filters-active");
    });

    $(".search-toggle").click(function () {
        $(".search-container").toggleClass("search-container-active");
    });

    $(".about,.about-modal-close").click(function () {
        $(".about-modal").toggleClass("about-modal-active");
    });


    $('#museum-filter').click(function() {
        if ($.inArray(1, APP.category) == -1 ) {
            $('#museum-filter').children('img').attr('src', '/static/img/museums-active.svg');
            APP.category.push(1);
        } else {
            $('#museum-filter').children('img').attr('src', '/static/img/museums.svg');
            APP.category.splice($.inArray(1, APP.category),1);
        }
        $APP.trigger('submitSearch');
    });

    $('#attractions-filter').click(function() {
        if ($.inArray(2, APP.category) == -1 ) {
            $('#attractions-filter').children('img').attr('src', '/static/img/attractions-active.svg');
            APP.category.push(2);
        } else {
            $('#attractions-filter').children('img').attr('src', '/static/img/attractions.svg');
            APP.category.splice($.inArray(2, APP.category),1);
        }
        $APP.trigger('submitSearch');
    });

    $('#animals-filter').click(function() {
        if ($.inArray(3, APP.category) == -1 ) {
            $('#animals-filter').children('img').attr('src', '/static/img/animals-active.svg');
            APP.category.push(3);
        } else {
            $('#animals-filter').children('img').attr('src', '/static/img/animals.svg');
            APP.category.splice($.inArray(3, APP.category),1);
        }
        $APP.trigger('submitSearch');
    });

    $('#parks-filter').click(function() {
        if ($.inArray(4, APP.category) == -1 ) {
            $('#parks-filter').children('img').attr('src', '/static/img/parks-active.svg');
            APP.category.push(4);
        } else {
            $('#parks-filter').children('img').attr('src', '/static/img/parks.svg');
            APP.category.splice($.inArray(4, APP.category),1);
        }
        $APP.trigger('submitSearch');
    });

    $('#sports-filter').click(function() {
        if ($.inArray(6, APP.category) == -1 ) {
            $('#sports-filter').children('img').attr('src', '/static/img/sports-active.svg');
            APP.category.push(6);
        } else {
            $('#sports-filter').children('img').attr('src', '/static/img/sports.svg');
            APP.category.splice($.inArray(6, APP.category),1);
        }
        $APP.trigger('submitSearch');
    });

    $('#events-filter').click(function() {
        if ($.inArray(10, APP.category) == -1 ) {
            $('#events-filter').children('img').attr('src', '/static/img/events-active.svg');
            APP.category.push(10);
        } else {
            $('#events-filter').children('img').attr('src', '/static/img/events.svg');
            APP.category.splice($.inArray(10, APP.category),1);
        }
        $APP.trigger('submitSearch');
    });

});