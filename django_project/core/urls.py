from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns(
    '',

    # Enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    # Examples:
    # url(r'^brb/', include('brb.foo.urls')),

    url(r'', include('backend.urls')),
    url(r'', include('frontend.urls')),
    # required by django-select2
    url(r'^select2/', include('django_select2.urls')),
    url(r'^files-widget/', include('topnotchdev.files_widget.urls')),
)

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT})
    )
