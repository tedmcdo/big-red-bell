Django==1.6.4
South==0.8.4
psycopg2==2.5.2
django-crispy-forms==1.4.0
django-braces==1.4.0
django-model-utils==2.0.3
django-pipeline==1.3.24
Django-Select2==4.2.2
django-tastypie==0.11.0
django-haystack==2.1.0
Pillow==2.4.0
git+git://github.com/TND/django-files-widget.git
git+git://github.com/notanumber/xapian-haystack.git
# raven==3.3.7