from django.conf.urls import patterns, url
from .views import (
    PoiList,
    PoiEdit,
    PoiAdd,
    GetRelatedWorktime,
    AdAdd,
    AdList,
    AdEdit,
    CategoriesList,
    CategoriesEdit,
    CategoriesAdd,
    DeletePoi,
    DeleteAd,
    DeleteCategory,
    LoginBackend,
    LogoutBackend,
    GetRelatedPrices,
    FeatureAdd,
    FeatureList,
    FeatureEdit,
    DeleteFeature,
    AmenityAdd,
    AmenityList,
    AmenityEdit,
    DeleteAmenity,
    PlaceList,
    PlaceAdd,
    PlaceEdit,
    DeletePlace
)

urlpatterns = patterns(
    '',
    # basic app views
    # url(r'^...', a_view)
    url(r'^cp/$', PoiList.as_view(), name='PoiList'),
    url(r'^cp/edit/(?P<pk>\d+)/$', PoiEdit.as_view(), name='PoiEdit'),
    url(r'^cp/add/$', PoiAdd.as_view(), name='PoiAdd'),
    url(r'^cp/get_worktime/$', GetRelatedWorktime.as_view()),
    url(r'^cp/add_ad/$', AdAdd.as_view(), name='AdAdd'),
    url(r'^cp/list_ad/$', AdList.as_view(), name='AdList'),
    url(r'^cp/edit_ad/(?P<pk>\d+)/$', AdEdit.as_view(), name='AdEdit'),
    url(r'^cp/list_cat/$', CategoriesList.as_view(), name='CategoriesList'),
    url(r'^cp/add_cat/$', CategoriesAdd.as_view(), name='CategoriesAdd'),
    url(
        r'^cp/edit_cat/(?P<pk>\d+)/$',
        CategoriesEdit.as_view(),
        name='CategoriesEdit'
    ),
    url(r'^cp/del/poi/(?P<pk>\d+)/$', DeletePoi.as_view(), name='PoiDel'),
    url(r'^cp/del/ad/(?P<pk>\d+)/$', DeleteAd.as_view(), name='AdDel'),
    url(r'^cp/del/cat/(?P<pk>\d+)/$', DeleteCategory.as_view(), name='CatDel'),
    url(r'^cp/login$', LoginBackend.as_view(), name='LoginBackend'),
    url(r'^cp/logout$', LogoutBackend.as_view(), name='LogoutBackend'),
    url(r'^cp/get_prices/$', GetRelatedPrices.as_view()),
    url(r'^cp/add_feature/$', FeatureAdd.as_view(), name='FeatureAdd'),
    url(r'^cp/list_feature/$', FeatureList.as_view(), name='FeatureList'),
    url(
        r'^cp/edit_feature/(?P<pk>\d+)/$',
        FeatureEdit.as_view(),
        name='FeatureEdit'
    ),
    url(
        r'^cp/del/feature/(?P<pk>\d+)/$',
        DeleteFeature.as_view(),
        name='FeatureDel'
    ),
    url(r'^cp/add_amenity/$', AmenityAdd.as_view(), name='AmenityAdd'),
    url(r'^cp/list_amenity/$', AmenityList.as_view(), name='AmenityList'),
    url(
        r'^cp/edit_amenity/(?P<pk>\d+)/$',
        AmenityEdit.as_view(),
        name='AmenityEdit'
    ),
    url(
        r'^cp/del/amenity/(?P<pk>\d+)/$',
        DeleteAmenity.as_view(),
        name='AmenityDel'
    ),

    url(r'^cp/list_place/$', PlaceList.as_view(), name='PlaceList'),
    url(r'^cp/add_place/$', PlaceAdd.as_view(), name='PlaceAdd'),
    url(
        r'^cp/edit_place/(?P<pk>\d+)/$',
        PlaceEdit.as_view(),
        name='PlaceEdit'
    ),
    url(
        r'^cp/del/place/(?P<pk>\d+)/$',
        DeletePlace.as_view(),
        name='PlaceDel'
    ),
)
