import logging
logger = logging.getLogger(__name__)

import json
from datetime import datetime

from django.http import (
    HttpResponseRedirect,
    HttpResponseBadRequest
)
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy
from django.views.generic import (
    ListView,
    FormView,
    UpdateView,
    TemplateView,
    DeleteView,
    RedirectView,
)
from django.contrib.auth import authenticate, login, logout
from django.core.serializers import serialize

from .models import (
    POI,
    Worktime,
    Ad,
    Categories,
    Price,
    FeaturePanel,
    Amenity,
    Place
)
from .forms import (
    POIForm,
    AdForm,
    CategoriesForm,
    LoginBackend,
    FeaturePanelForm,
    AmenityForm,
    PlaceForm
)

from braces.views import (
    JSONResponseMixin,
    LoginRequiredMixin,
    StaffuserRequiredMixin
)

LOGIN_URL = 'LoginBackend'


class PoiList(LoginRequiredMixin, StaffuserRequiredMixin, ListView):
    context_object_name = 'pois'
    model = POI
    template_name = 'backend-poilist.html'
    login_url = LOGIN_URL


class PoiEdit(LoginRequiredMixin, StaffuserRequiredMixin, UpdateView):
    form_class = POIForm
    template_name = 'backend-poiform.html'
    success_url = reverse_lazy('PoiList')
    model = POI
    login_url = LOGIN_URL

    def form_valid(self, form):
        ob = form.save()

        #Get worktime dict from hidden field.
        worktime = json.loads(form.cleaned_data['worktime'])

        worktimeSet = ob.worktime_set.all()

        #Update existing worktime objects.
        for day in worktime:
            worktimeObject = worktimeSet.get(dow=day)

            start = worktime[day]['start']
            end = worktime[day]['end']

            #Time module doesn't have 'strptime', but datetime does...
            startDateTime = datetime.strptime(start, '%H:%M')
            startTime = startDateTime.time()
            endDateTime = datetime.strptime(end, '%H:%M')
            endTime = endDateTime.time()

            worktimeObject.startTime = startTime
            worktimeObject.endTime = endTime
            worktimeObject.save()

        #Get price dict from hidden field.
        priceData = json.loads(form.cleaned_data['price'])

        price = ob.price_set.get()

        price.toddler = priceData['toddler']
        price.children = priceData['children']
        price.adults = priceData['adults']
        price.students = priceData['students']
        price.seniors = priceData['seniors']
        price.save()

        return HttpResponseRedirect(self.get_success_url())


class PoiAdd(LoginRequiredMixin, StaffuserRequiredMixin, FormView):
    form_class = POIForm
    template_name = 'backend-poiform.html'
    success_url = reverse_lazy('PoiList')
    login_url = LOGIN_URL

    def form_valid(self, form):
        ob = form.save()

        #Get worktime dict from hidden field.
        worktime = json.loads(form.cleaned_data['worktime'])

        for day in worktime:
            start = worktime[day]['start']
            end = worktime[day]['end']

            #Time module doesn't have 'strptime', but datetime does...
            startDateTime = datetime.strptime(start, '%H:%M')
            startTime = startDateTime.time()
            endDateTime = datetime.strptime(end, '%H:%M')
            endTime = endDateTime.time()

            Worktime(
                poiid=ob,
                dow=int(day),
                startTime=startTime,
                endTime=endTime
            ).save()

        #Get price dict from hidden field.
        price = json.loads(form.cleaned_data['price'])

        Price(
            poi=ob,
            toddler=price['toddler'],
            children=price['children'],
            adults=price['adults'],
            students=price['students'],
            seniors=price['seniors'],
        ).save()

        return HttpResponseRedirect(self.get_success_url())


class GetRelatedWorktime(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    JSONResponseMixin,
    TemplateView
):
    def get(self, request, *args, **kwargs):
        objectID = request.GET.get('ObID', False)

        if objectID is False:
            return HttpResponseBadRequest()

        #Get worktime objects for objectID.
        worktimeSet = Worktime.objects.filter(poiid=objectID)

        #Extract and prepare data, if it exists.
        result = {}
        for ob in worktimeSet:
            startTime = ob.startTime.strftime('%H:%M')
            endTime = ob.endTime.strftime('%H:%M')
            result[ob.dow] = {'start': startTime, 'end': endTime}

        return self.render_json_response(result)


class GetRelatedPrices(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    JSONResponseMixin,
    TemplateView
):
    def get(self, request, *args, **kwargs):
        objectID = request.GET.get('ObID', False)

        if objectID is False:
            return HttpResponseBadRequest()

        price = Price.objects.filter(poi=objectID)
        data = serialize('json', price)

        return self.render_json_response(data)


class AdList(LoginRequiredMixin, StaffuserRequiredMixin, ListView):
    context_object_name = 'ads'
    model = Ad
    template_name = 'backend-adlist.html'
    login_url = LOGIN_URL


class AdEdit(LoginRequiredMixin, StaffuserRequiredMixin, UpdateView):
    form_class = AdForm
    template_name = 'backend-adform.html'
    success_url = reverse_lazy('AdList')
    model = Ad
    login_url = LOGIN_URL

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(self.get_success_url())


class AdAdd(LoginRequiredMixin, StaffuserRequiredMixin, FormView):
    form_class = AdForm
    template_name = 'backend-adform.html'
    success_url = reverse_lazy('AdList')
    login_url = LOGIN_URL

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(self.get_success_url())


class CategoriesList(LoginRequiredMixin, StaffuserRequiredMixin, ListView):
    context_object_name = 'categories'
    model = Categories
    template_name = 'backend-categories_list.html'
    login_url = LOGIN_URL


class CategoriesEdit(LoginRequiredMixin, StaffuserRequiredMixin, UpdateView):
    form_class = CategoriesForm
    template_name = 'backend-categories_form.html'
    success_url = reverse_lazy('CategoriesList')
    model = Categories
    login_url = LOGIN_URL

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(self.get_success_url())


class CategoriesAdd(LoginRequiredMixin, StaffuserRequiredMixin, FormView):
    form_class = CategoriesForm
    template_name = 'backend-categories_form.html'
    success_url = reverse_lazy('CategoriesList')
    login_url = LOGIN_URL

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(self.get_success_url())


class DeletePoi(LoginRequiredMixin, StaffuserRequiredMixin, DeleteView):
    model = POI
    success_url = reverse_lazy('PoiList')
    template_name = 'backend-poi-delete.html'
    login_url = LOGIN_URL


class DeleteAd(LoginRequiredMixin, StaffuserRequiredMixin, DeleteView):
    model = Ad
    success_url = reverse_lazy('AdList')
    template_name = 'backend-ad-delete.html'
    login_url = LOGIN_URL


class DeleteCategory(LoginRequiredMixin, StaffuserRequiredMixin, DeleteView):
    model = Categories
    success_url = reverse_lazy('CategoriesList')
    template_name = 'backend-cat-delete.html'
    login_url = LOGIN_URL


class LoginBackend(FormView):
    template_name = 'backend-login.html'
    form_class = LoginBackend
    success_url = reverse_lazy('PoiList')

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(self.request, user)
            return redirect('PoiList')


class LogoutBackend(RedirectView):
    url = reverse_lazy('LoginBackend')

    def post(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutBackend, self).post(request, *args, **kwargs)


class FeatureList(LoginRequiredMixin, StaffuserRequiredMixin, ListView):
    context_object_name = 'features'
    model = FeaturePanel
    template_name = 'backend-featurelist.html'
    login_url = LOGIN_URL


class FeatureEdit(LoginRequiredMixin, StaffuserRequiredMixin, UpdateView):
    form_class = FeaturePanelForm
    template_name = 'backend-featureform.html'
    success_url = reverse_lazy('FeatureList')
    model = FeaturePanel
    login_url = LOGIN_URL

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(self.get_success_url())


class FeatureAdd(LoginRequiredMixin, StaffuserRequiredMixin, FormView):
    form_class = FeaturePanelForm
    template_name = 'backend-featureform.html'
    success_url = reverse_lazy('FeatureList')
    login_url = LOGIN_URL

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(self.get_success_url())


class DeleteFeature(LoginRequiredMixin, StaffuserRequiredMixin, DeleteView):
    model = FeaturePanel
    success_url = reverse_lazy('FeatureList')
    template_name = 'backend-feature-delete.html'
    login_url = LOGIN_URL


class AmenityList(LoginRequiredMixin, StaffuserRequiredMixin, ListView):
    context_object_name = 'amenities'
    model = Amenity
    template_name = 'backend-amenitylist.html'
    login_url = LOGIN_URL


class AmenityEdit(LoginRequiredMixin, StaffuserRequiredMixin, UpdateView):
    form_class = AmenityForm
    template_name = 'backend-amenityform.html'
    success_url = reverse_lazy('AmenityList')
    model = Amenity
    login_url = LOGIN_URL

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(self.get_success_url())


class AmenityAdd(LoginRequiredMixin, StaffuserRequiredMixin, FormView):
    form_class = AmenityForm
    template_name = 'backend-amenityform.html'
    success_url = reverse_lazy('AmenityList')
    login_url = LOGIN_URL

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(self.get_success_url())


class DeleteAmenity(LoginRequiredMixin, StaffuserRequiredMixin, DeleteView):
    model = Amenity
    success_url = reverse_lazy('AmenityList')
    template_name = 'backend-amenity-delete.html'
    login_url = LOGIN_URL


class PlaceList(LoginRequiredMixin, StaffuserRequiredMixin, ListView):
    context_object_name = 'places'
    model = Place
    template_name = 'backend-placeslist.html'
    login_url = LOGIN_URL


class PlaceEdit(LoginRequiredMixin, StaffuserRequiredMixin, UpdateView):
    form_class = PlaceForm
    template_name = 'backend-placeform.html'
    success_url = reverse_lazy('PlaceList')
    model = Place
    login_url = LOGIN_URL

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(self.get_success_url())


class PlaceAdd(LoginRequiredMixin, StaffuserRequiredMixin, FormView):
    form_class = PlaceForm
    template_name = 'backend-placeform.html'
    success_url = reverse_lazy('PlaceList')
    login_url = LOGIN_URL

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(self.get_success_url())


class DeletePlace(LoginRequiredMixin, StaffuserRequiredMixin, DeleteView):
    model = Place
    success_url = reverse_lazy('PlaceList')
    template_name = 'backend-place-delete.html'
    login_url = LOGIN_URL
