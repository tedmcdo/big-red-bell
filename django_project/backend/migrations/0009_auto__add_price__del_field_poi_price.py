# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Price'
        db.create_table(u'backend_price', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('children', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('adults', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('students', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('seniors', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('poi', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.POI'])),
        ))
        db.send_create_signal(u'backend', ['Price'])

        # Deleting field 'POI.price'
        db.delete_column(u'backend_poi', 'price')


    def backwards(self, orm):
        # Deleting model 'Price'
        db.delete_table(u'backend_price')

        # Adding field 'POI.price'
        db.add_column(u'backend_poi', 'price',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0),
                      keep_default=False)


    models = {
        u'backend.ad': {
            'Meta': {'object_name': 'Ad'},
            'areaOfInterest': ('django.contrib.gis.db.models.fields.PolygonField', [], {}),
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['backend.Categories']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'images': ('topnotchdev.files_widget.fields.ImageField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'keywords': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['backend.Keywords']", 'symmetrical': 'False'}),
            'location': ('django.contrib.gis.db.models.fields.PointField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'})
        },
        u'backend.categories': {
            'Meta': {'object_name': 'Categories'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'backend.keywords': {
            'Meta': {'object_name': 'Keywords'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'backend.poi': {
            'Meta': {'object_name': 'POI'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'areaOfInterest': ('django.contrib.gis.db.models.fields.PolygonField', [], {}),
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['backend.Categories']", 'symmetrical': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'images': ('topnotchdev.files_widget.fields.ImagesField', [], {'null': 'True', 'blank': 'True'}),
            'keywords': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['backend.Keywords']", 'symmetrical': 'False'}),
            'location': ('django.contrib.gis.db.models.fields.PointField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'backend.price': {
            'Meta': {'object_name': 'Price'},
            'adults': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'children': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'poi': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.POI']"}),
            'seniors': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'students': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'backend.worktime': {
            'Meta': {'object_name': 'Worktime'},
            'dow': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'endTime': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'poiid': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.POI']"}),
            'startTime': ('django.db.models.fields.TimeField', [], {})
        }
    }

    complete_apps = ['backend']