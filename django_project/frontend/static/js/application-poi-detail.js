APP.dontReloadNext = false;
APP.searchActive = false;
APP.selectedPOI = false;

APP.Place = Backbone.Model.extend({
    url:'/api/v1/place/'
});

APP.PlaceCollection = Backbone.Collection.extend({
    url: '/api/v1/place/',
    model: APP.Place,
    meta: {},

    parse: function(response) {
        this.meta = response.meta;
        return response.objects;
    },
});

APP.PlaceItemView = Backbone.View.extend({
    events: {
        'mouseover': "highlightPOI",
        'mouseout': "unhighlightPOI",
    },
    initialize: function() {
        this.popup=document.createElement('div');
        this.popup.className='popup';
    },
    renderDetail: function() {
        var html = JST.place_detail_tmpl({model: this.model});
        $APP.trigger('addPOIDetail', {'html': html});
    },
    showDetailed: function() {
        var selected = null;
        if (APP.selectedPOI != null) {
            // fetch selected model from collection
            selected = placeview.collection.get(APP.selectedPOI);
        }

        APP.selectedPOI = this.model.get('id');
        if (selected) selected.marker.myItem.unhighlightPOI();
        this.highlightPOI();
        APP.dontReloadNext = true;
        this.renderDetail();

        // Hide informative text.
        $('#poi-informative-text').hide();
    },
    hideDetailed: function() {
        $('#poi-place-description').empty();
        $('#poi-informative-text').show();
    },
    highlightPOI: function() {
        this.fixPopupError();
        this.model.marker.setIcon(L.divIcon({
            className: 'count-icon-highlight-new',
            iconAnchor: [21,43],
            iconSize: [43, 47]
        }));
        var parent = placeview.markerLayer.getVisibleParent(this.model.marker);
        if (_.isUndefined(parent._group)) {
            this.showPopup();
        } else {
            $(parent._icon).addClass('active');
        }
    },
    fixPopupError: function() {
        // Check if popup exists before highlighting. If it exits it means that things
        // got messed up during map move (eg. click on cluster) so popup was
        // never removed. We remove it here.
        if ($('.popup').length > 0) {
            $('.popup').remove();
        }
    },
    unhighlightPOI: function() {
        this.model.marker.setIcon(L.divIcon({
            className: 'count-icon-new',
            iconAnchor: [21,43],
            iconSize: [43, 47]
        }));
        var parent = placeview.markerLayer.getVisibleParent(this.model.marker);
        if (_.isUndefined(parent._group)) {
            $(this.popup).remove();
        } else {
            $(parent._icon).removeClass('active');
        }
    },
    showPopup: function() {
        $('#poi-map').append(this.popup);
        var ll = this.model.marker.getLatLng();
        var point = APP.map.latLngToContainerPoint(ll);
        var html = '<p class="popup_elem">'+this.model.attributes.name+'</p>';
        $(this.popup).css(
            {
                'top':(point.y- 90) +'px',
                'left': point.x-100 + 'px'
            }
        );
        $(this.popup).html(html);
    },
});

APP.PlaceView = Backbone.View.extend({
    el: $(".main-panel-map"),

    initialize: function(options) {
        // isMapInitialized is set to 'true' after markers are shown after initialization.
        APP.isMapInitialized = false;
        this.collection = new APP.PlaceCollection();
        this.collection.bind('reset', this.render, this);
        this.collection.bind('remove', this.render, this);
        $APP.on('addPOIDetail', $.proxy(this.addPOIDetail, this));
        if ($( window ).width() < 860) {
            APP.mobile = true;
            APP.sidebarWidthPoint = [0, 0];
            APP.sidebarHeightPoint = [0, 0];
        } else {
            APP.mobile = false;
            APP.sidebarWidthPoint = L.point(0, 0);
        }
        this.initMap();
        APP.selectedPOI = null;
    },

    render: function() {
        if (_.isUndefined(this.markerLayer)) {
            this.markerLayer = new L.MarkerClusterGroup({
                showCoverageOnHover: false,
                iconCreateFunction: function(cluster) {
                    return new L.divIcon({
                        className: 'count-icon-cluster',
                        html: '<em class="clusterChildCount">'+cluster.getChildCount()+'</em>',
                        iconAnchor: [25,49],
                        iconSize: [47, 51]
                     });
                }
            });
        } else {
            this.markerLayer.clearLayers();
        }

        this.number = 0;
        var self=this;

        if (this.collection.models.length > this.number ) {
            var start = this.number;
            for (var i = start; i < this.collection.models.length; i++) {
                var item = this.collection.models[i];
                item.attributes.number = this.number + 1;
                myItem = this.renderItem(item);
                item.marker = this.renderMarker(item);
                item.marker.myItem = myItem;
                this.number += 1;
                if (APP.selectedPOI == myItem.model.get('id')) {
                    myItem.highlightPOI();
                }
            }
        }
        this.markerLayer.addTo(this.map);
        $('.la-anim').removeClass('la-animate');

        // Only if map has just been initialized do next...
        if (APP.isMapInitialized == false) {
            APP.isMapInitialized = true;
            this.fitMapView(this.markerLayer);
        }
        return this;
    },

    renderItem: function(item) {
        var myItem = new APP.PlaceItemView({
            model:item,
            collection:this.collection
        });

        return myItem;
    },

    renderMarker: function(item) {
        var data = item.pick('id', 'name', 'location', 'number');
        //Omnivore returns layer which contains marker we need to set custom marker...
        var wkt = omnivore.wkt.parse(data.location);
        //Get marker.
        var layer = wkt.getLayers()[0];
        //As we can't use layer.setIcon without breaking something, construct a new marker.
        var marker = new L.Marker(layer.getLatLng(), {
             icon: L.divIcon({
                className: 'count-icon-new',
                iconAnchor: [21,43],
                iconSize: [43, 47]

             })
        });
        marker.addTo(this.markerLayer);
        $(marker).hover(
            function() {
                this.myItem.highlightPOI();
                this.myItem.showDetailed();
            },
            function() {
                this.myItem.unhighlightPOI();
                this.myItem.hideDetailed();
            }
        );
        return marker;
    },

    addPOIDetail: function(event, data) {
        var self = this;
        $('#poi-place-description').html(data.html);
    },

    initMap: function() {
        var self = this;
        this.moved = 0;
        //Init map.
        this.map = L.mapbox.map('poi-map', 'tedmcdo.ippbgho2', {'minZoom': 12, 'touchZoom': true, 'tapTolerance': 25});

        //On any movement, get data from server and show it as markers on map.
        //Bound box is limited to visible map.
        this.map.on('moveend', function() {
            // if true, moveend is cause by show detailed and we don't want to reload
            if (!APP.dontReloadNext && !APP.searchActive) {
                self.loadPOIs()
            } else {
                // set to false as next action will be pan or zoom and we want reload to fire
                APP.dontReloadNext = false;
            }
        }, this);
        this.map.on('movestart', function() {
            $('.popup').remove();
        }, this);
        this.map.on('moveend', function() {
            $('.popup').remove();
        }, this);
        this.map.on('dragend', function(e) {
            this.moved =  this.moved + e.distance
            var limit = $( window ).width() * 0.25;
            if (this.moved < limit) {
                APP.dontReloadNext = true;
            } else {
                this.moved = 0;
            }
        }, this);

        //Set view AFTER 'load' listner is defined.
        this.map.fitBounds([
            [47.4819684, -122.459696],
            [47.7341388, -122.224433]
        ]);
        APP.map = this.map;
    },

    loadPOIs: function() {
        $('.la-anim').addClass('la-animate');
        var self = this;
        var poiurl = this.collection.url;

        filters = {
          'poiID': APP.ReferentPOIId
        };

        // If map is not initialized (i.e. initially fetching places)
        // don't use bbox for filtering.
        if (APP.isMapInitialized == true) {
            bbox = this.getBBOX();
            filters['bbox'] = bbox;
        };

        self.collection.fetch({
          'reset':true,
          data: filters
        });

        self.collection.url = poiurl;
    },

    getBBOX: function() {
        var mapBounds = this.map.getBounds();
        return mapBounds.toBBoxString();
    },

    fitMapView: function(layer) {
        // Fit to layer bounds if there are markers on layer.
        if (layer.getLayers().length != 0) {
            bounds = layer.getBounds();
            this.map.fitBounds(bounds);
        }

    }
});

jQuery(document).ready(function($) {
  $('#lightSlider').lightSlider({
    gallery: true,
    item: 1,
    slideMargin: 0,
    galleryMargin: 10,
    thumbMargin: 10,
    thumbItem: 10,
    responsive : [
      {
        breakpoint:760,
        settings: {
          slideMargin: 0,
          galleryMargin: 10,
          thumbMargin: 10,
          thumbItem: 6
        }
      },
      {
        breakpoint:480,
        settings: {
          slideMargin: 0,
          galleryMargin: 10,
          thumbMargin: 10,
          thumbItem: 4
        }
      }
    ]
  });
});